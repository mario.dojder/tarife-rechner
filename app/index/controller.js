import Controller from '@ember/controller';
import { observer, get, set } from '@ember/object';
import { match } from '@ember/object/computed';
// import Ember from 'ember';
// const {
//     get,
//     set
//   } = Ember;

export default Controller.extend({
    zarada: null,
    serviserIznos: null,
    tarifa: null,
    isDisabled: true,
    jeRadioOznacen: false,
    isValid: match('ukupniPromet', /^[0-9]+$/),

    aktualnaTarifa: observer('selectedNumber', function() {
        if(get(this, 'isValid')) {
            set(this, 'isDisabled', false);
        } else {
            set(this, 'isDisabled', true);
        }
        set(this, 'jeRadioOznacen', true);
    }),
    aktualniPromet: observer('ukupniPromet', function() {
        if(get(this, 'isValid')) {
            if(get(this, 'jeRadioOznacen')) {
                set(this, 'isDisabled', false);
            }
        } else {
            set(this, 'isDisabled', true);
        }
    }),

    actions: {
        Izracunaj(ukupniPromet){
            if(!isNaN(ukupniPromet)){
                if(get(this, 'selectedNumber')){
                    if(get(this, 'selectedNumber') == 80){
                        set(this, 'serviserIznos', 600);
                        set(this, 'tarifa', 0.2);
                    } else if(get(this, 'selectedNumber') == 70){
                        set(this, 'serviserIznos', 1200);
                        set(this, 'tarifa', 0.3);
                    } else if(get(this, 'selectedNumber') == 60){
                        set(this, 'serviserIznos', 1980);
                        set(this, 'tarifa', 0.4);
                    }
                    
                    set(this, 'zarada', ukupniPromet * get(this, 'tarifa'));
                }
                
            }
        }
    }
});
